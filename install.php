<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<html>
<head>
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/gh/rastikerdar/shabnam-font@v5.0.1/dist/font-face.css">
    <style>
        body{
            font-family: Shabnam, Tahoma, "DejaVu Sans", helvetica, arial, freesans, sans-serif;
            direction: rtl;
            margin: 50px 150px 0px 150px;
        }
        input{
            font-family: Shabnam;
            text-align: left;
        }
        input[type=text]{
            width: 400px;
            direction: ltr;
        }
        .error{
            padding: 10px;
            background-color: #FFBFBF;
            border-radius: 10px;
            margin-bottom: 20px;
        }
        .titleBar{
            font-weight: bold;
            color: #0057E1;
            margin-bottom: 20px;
        }
        .code{
            direction: ltr;
            text-align: left;
            background-color: black;
            color: white;
            margin: 10px 0px 10px 0px;
            padding: 10px;
            border-radius: 5px;
        }
        input[type=submit]{
            border-radius: 5px;
            border: none;
            background-color: #93CEA0;
            font-weight: bold;
            padding: 5px 20px 5px 20px;
        }
        input[type=submit]:hover{
            background-color: #B0DCBA;
        }
    </style>
    <title>نصب ربات فیواستار</title>
</head>
<body>
    <?php
    if(empty($_GET['step']) || $_GET['step']=='db')
    {
    ?>
        <div class="titleBar">#دیتابیس</div>
        <form method="post">
            نام دیتابیس<br>
            <input type="text" name="dbName" value="<?php echo @$_POST['dbName']; ?>"><br><br>
            یوزرنیم دیتابیس<br>
            <input type="text" name="dbUser" value="<?php echo @$_POST['dbUser']; ?>"><br><br>
            پسورد دیتابیس<br>
            <input type="text" name="dbPass" value="<?php echo @$_POST['dbPass']; ?>"><br><br>
            <input type="submit" value="ثبت">
        </form>
    <?php
        if(isset($_POST['dbName']) && isset($_POST['dbUser']) && isset($_POST['dbPass']))
        {
            $db = @ new mysqli('localhost', $_POST['dbUser'], $_POST['dbPass'], $_POST['dbName']); 
            if($db->connect_error)
            {
                echo '<div class="error">خطا در اتصال به دیتابیس: ' . mysqli_connect_error().'</div>';
            }
            else
            {
                file_put_contents('config.php', str_replace(['[DBNAME]', '[DBPASS]', '[DBUSER]'], [$_POST['dbName'], $_POST['dbPass'], $_POST['dbUser']], file_get_contents('config.php')));
                $query = '';
                $sqlScript = file('DB.sql');
                foreach ($sqlScript as $line)
                {
                    $startWith = substr(trim($line), 0 ,2);
                	$endWith = substr(trim($line), -1 ,1);
                	if(empty($line) || $startWith == '--' || $startWith == '/*' || $startWith == '//') {
                		continue;
                	}
                	$query = $query . $line;
                	if($endWith == ';'){
                		mysqli_query($db,$query) or die('<div class="error">Problem in executing the SQL query <b>' . $query. '</b></div>');
                		$query= '';		
                	}
                }
                echo '<meta http-equiv="refresh" content="0;url=?step=telegram">';
            }
        }
    }
    else if($_GET['step']=='telegram')
    {
    ?>
        <div class="titleBar">#تلگرام</div>
        <form method="post">
            توکن ربات<br>
            <input type="text" name="token" value="<?php echo @$_POST['token']; ?>" placeholder="000:xxx"><br><br>
            یوزرنیم ادمین اصلی<br><small>بدون @ وارد کنید</small><br>
            <input type="text" name="adminUsername" value="<?php echo @$_POST['adminUsername']; ?>" placeholder="yaqoubinet"><br><br>
            لینک فایل bot.php<br><small>شروع با https://</small><br><small>مثال: https://domain.com/.../bot.php</small><br>
            <input type="text" name="botPhpLink" value="<?php echo (empty($_POST['botPhpLink'])) ? str_replace('install.php', 'bot.php', "https://".$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME']) : $_POST['botPhpLink']; ?>" placeholder="https://domain.com/.../bot.php"><br><br>
            <input type="submit" value="ثبت">
        </form>
    <?php
        if(!empty($_POST['adminUsername']) && !empty($_POST['botPhpLink']) && !empty($_POST['token']))
        {
            include 'config.php';
            $db = @ new mysqli('localhost', $db_user, $db_pass, $db_name);
            $token = $_POST['token'];
            $pullTweetsUrl = str_replace('bot.php', 'pullTweets.php', $_POST['botPhpLink']);
            $autoSendUrl   = str_replace('bot.php', 'autoSend.php', $_POST['botPhpLink']);
            $username = strtolower($_POST['adminUsername']);
            if(
                file_get_contents("https://api.telegram.org/bot".$_POST['token']."/setWebHook?url=".$_POST['botPhpLink']) &&
                $db->query("INSERT INTO tel_admins (username, access_level) VALUES ('$username', '3')") &&
                $db->query("UPDATE settings SET value='$token' WHERE name='botToken'") &&
                file_put_contents('botCron.sh', str_replace(['[PULLTWEETSURL]', '[AUTOSENDURL]'], [$pullTweetsUrl, $autoSendUrl], file_get_contents('botCron.sh')))
            )
            {
                echo '<meta http-equiv="refresh" content="0;url=?step=cron">';
            }
            else
            {
                echo '<div class="error">خطایی رخ داده است.</div>';
            }
        }
    }
    else if($_GET['step']=='cron')
    {
    ?>
        <div class="titleBar">#کران جاب</div>
        <p>
            کار نصب <b>تقریبا</b> به پایان رسید. 
            اکنون کافیست یکی از دو مورد زیر را انجام دهید.
        </p>
        <ul>
            <li>اجرای فایل botCron.sh از طریق دستور زیر: <div class="code"><code>sh botCron.sh &</code></div></li>
            <li>تنظیم فراخوانی کران جاب برای دو فایل <b>autoSend.php</b> و <b>pullTweets.php</b> با حداکثر تعداد فراخوانی ممکن.<br>
            نمونه:<br>
            <div class="code"><code>curl https://domain.com/.../autoSend.php</code></div>
            <div class="code"><code>curl https://domain.com/.../pullTweets.php</code></div>
            </li>
        </ul>
    <?php
    }
    ?>
</body>
</html>