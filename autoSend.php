<?php

include('database.php');

$db = new database();

if ($db->settings('activation') && (time()-$db->settings('dateOfLastPost'))/60 > $db->settings('pauseBetweenPosts')) 
{
    $channel = $db->settings('targetID');
    for($i=0 ; $i<$db->settings('numOfTweetsPerPost') ; $i++)
        if($db->sendToTelegram($channel, 'channel', ['is_sent'=>1]))
            $db->settings('dateOfLastPost', time());
        else
            break;
}

$db->close();