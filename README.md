# Favstar

auto post persian tweets from twitter to a telegram channel

پست کردن خودکار توییت‌های فارسی از توییتر به کانال تلگرام

## Installation
upload the package to your server, then run the `install.php` file.

برای نصب کافیست فایل‌های اسکریپت را در سرور خود آپلود کرده و فایل `install.php` را اجرا کنید.

## Screenshots
<img src="https://yaqoubi.net/img/screenshots/photo_2020-07-12_16-05-51.jpg" width="400">
<img src="https://yaqoubi.net/img/screenshots/photo_2020-07-12_16-05-43.jpg" width="400">
<img src="https://yaqoubi.net/img/screenshots/photo_2020-07-12_16-05-35.jpg" width="400">
<img src="https://yaqoubi.net/img/screenshots/photo_2020-07-12_16-05-45.jpg" width="400">
<img src="https://yaqoubi.net/img/screenshots/photo_2020-07-12_16-05-50.jpg" width="400">
<img src="https://yaqoubi.net/img/screenshots/photo_2020-07-12_16-06-46.jpg" width="400">
<img src="https://yaqoubi.net/img/screenshots/photo_2020-07-12_16-06-51.jpg" width="400">

**[Yaqoubi.net](https://yaqoubi.net)**
