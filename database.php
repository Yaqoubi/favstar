<?php
include 'config.php';
include 'telegram.php';

class database {
    public $conn;
    public function __construct()
    {
        global $db_user;
        global $db_pass;
        global $db_name;
        global $db_host;
        $this->conn = new mysqli($db_host, $db_user, $db_pass, $db_name); 
        $this->conn->set_charset('utf8mb4');
        if($this->conn->connect_error)
        {
            echo "Database Connection Failed: " . mysqli_connect_error();
            exit;
        }
    }


    function query($sql)
    {
        $result = $this->conn->query($sql);
        return ($result) ? $result : false;
    }

    function rows($table, $col=false)
    {
        $sql = "SELECT * FROM $table";
        $result = $this->conn->query($sql);
        while($row = $result->fetch_assoc())
            $array[] = ($col) ? $row[$col]: $row;
        return $array;
    }

    function settings($name, $value='read')
    {
        if($value == 'read')
        {
            $sql = "SELECT * FROM settings WHERE name='$name'";
            $result = $this->conn->query($sql);
            return ($result = mysqli_fetch_assoc($result)) ? $result['value'] : false;
        }
        else
        {
            $sql = "INSERT INTO settings (name, value) VALUES ('$name', '$value') ON DUPLICATE KEY UPDATE value = '$value'";
            return ($this->conn->query($sql) === TRUE) ? true : false;
        }
    }
    
    function isFreshTweet($tweet_id)
    {
        $sql = "SELECT tweet_id FROM tweets WHERE tweet_id='$tweet_id'";
        $result = $this->conn->query($sql);
        return ($result->num_rows == 0) ? true : false; 
    }
    
    function addRecord($table, $data)
    {
        $sql = "INSERT INTO $table (". implode(',', array_keys($data)) .")
        VALUES ('" . implode("', '", $data) . "')";
        return ($this->conn->query($sql) === TRUE) ? true : false;
    }
    
    function isAdmin($username, $user_id, $id = null)
    {
        $username = strtolower($username);
        if(!empty($id))
            $sql = "SELECT * FROM tel_admins WHERE id=$id";
        else if(!empty($user_id) && !empty($username))
            $sql = "SELECT * FROM tel_admins WHERE user_id='$user_id' OR username='$username'";
        $result = $this->conn->query($sql);
        if($result->num_rows==0) return false;
        else
        {
            $admin = $result->fetch_assoc();
            return $admin['access_level'];
        }
    }

    function numRows($table, $command=false)
    {
        $sql = (!$command) ? "SELECT * FROM $table" : "SELECT * FROM $table WHERE $command";
        $result = $this->conn->query($sql);
        return $result->num_rows;
    }
    
    function getConfirmPendingID($chat_id)
    {
        $arr = $this->rows('tel_admins');
        foreach ($arr as $key)
            if($key['username'] == strtolower($chat_id) ||
            $key['user_id'] == $chat_id)
                return $key['confirm_pending_id'];
        return false;
    }
    
    function deleteRow($table, $column, $value)
    {
        $sql = "DELETE FROM $table WHERE $column='$value'";
        return ($this->conn->query($sql)) ? true : false;
    }
    
    function dateCmp($created_at)
    {
        date_default_timezone_set('GMT');
        $time_limit = (float) $this->settings('minOfPublishTime');
        return ((time()-strtotime($created_at)) <= ($time_limit*60*60)) ? true : false;
    }
    
    function updateTable($table, $col, $value, $secCol, $secVal)
    {
        $sql = ($secCol) ? "UPDATE `$table` SET `$col`='$value' WHERE `$secCol`='$secVal'" : "UPDATE `$table` SET `$col`='$value'";
        if($this->conn->query($sql) === TRUE)
            return true;
        else
            return false;
    }
    
    function truncateTable($table)
    {
        $sql = "TRUNCATE TABLE $table";
        return ($this->conn->query($sql) === TRUE) ? true : false;
    }
    
    function censorship($username, $tweet)
    {
        $usernames_list = $this->rows('censored_users', 'username');
        $usernames_list = (!empty($usernames_list)) ? $usernames_list : false;
        $words_list     = $this->rows('censored_words', 'word');
        $words_list     = (!empty($words_list)) ? $words_list : false;
        $username       = strtolower($username);
        if(!empty($usernames_list[0]))
        {
            foreach($usernames_list as $username_list)
            {
                if($username_list == $username)
                {
                    return false;
                    break;
                }
            }
        }
        if(!empty($words_list[0]))
        {
            foreach($words_list as $word_list)
            {
                if(mb_strpos($tweet, $word_list) !== false)
                {
                    return false;
                    break;
                }
            }
        }
        return true;
    }
    
    function getTweet($target, $info=null /* $info = ['x'=>'y'] where x == y*/)
    {
        if($info == null)
        {
            if($target == 'bot')
            {
                $sql = "SELECT * FROM tweets WHERE seen=0 ORDER BY last_update ASC";
                $result = $this->conn->query($sql);
                $results = array();
                if($row = $result->fetch_assoc())
                    return $row;
                return false;
            }
            else if($target == 'channel')
            {
                $sql = "SELECT * FROM tweets WHERE confirmed=1 && is_sent=0 ORDER BY last_update ASC";
                $result = $this->conn->query($sql);
                $results = array();
                if($row = $result->fetch_assoc())
                    return $row;
                return false;
            }
            else
            {
                $sql = "SELECT * FROM tweets WHERE id=$target";
                $result = $this->conn->query($sql);
                $results = array();
                if($row = $result->fetch_assoc())
                    return $row;
                return false;
            }
        }
        else
        {
            $col_name = array_keys($info)[0];
            $col_val  = $info[$col_name];
            $sql = "SELECT * FROM tweets WHERE $col_name='$col_val'";
            $result = $this->conn->query($sql);
            if($row = $result->fetch_assoc())
                return $row;
            return false;
        }
    }

    function adminStat()
    {
        $admins_arr =  $this->rows('tel_admins');
        foreach($admins_arr as $admin)
            if($admin['user_id'])
                $admins[$admin['user_id']] = $admin['username'];
        $stats = null;
        $i=1;
        if(empty($admins))
            return false;
        foreach(array_keys($admins) as $user_id)
        {
            $sql = "SELECT id FROM tweets WHERE admin='$user_id'";
            $result = $this->conn->query($sql);
            $seen_count = $result->num_rows;
            $sql = "SELECT id FROM tweets WHERE admin='$user_id' AND confirmed=1";
            $result = $this->conn->query($sql);
            $confirmed_count = $result->num_rows;
            $percentage = ($seen_count==0) ? '0' : number_format($confirmed_count/$seen_count*100);
            $stats[$i]['username'] = $admins[$user_id];
            $stats[$i]['seen'] = $seen_count;
            $stats[$i]['confirmed'] = $confirmed_count;
            $stats[$i]['confirm_percentage'] = $percentage;
            $i++;
        }
        return $stats;
    }
    
    function aboutAdmin($username, $user_id)
    {
        $sql = "SELECT * FROM tel_admins WHERE username='$username' OR user_id='$user_id'";
        $result = $this->conn->query($sql);
        if($row = $result->fetch_assoc())
            return $row;
        else
            return false;
    }
    
    function sendToTelegram($target, $tweet_id, $updates=null)
    {
        $telegram = new Telegram($this->settings('botToken'));
        if($tweet_id=='bot') $tweet = $this->getTweet('bot');
        else if($tweet_id=='channel') $tweet = $this->getTweet('channel');
        else $tweet = $this->getTweet($tweet_id);
        if($tweet)
        {
            $webPerview = ($tweet['is_quote']) ? false : true;
            $tweet['tweet_text'] = ($tweet['is_quote'] || !empty($tweet['photo1'])) ? substr($tweet['tweet_text'], 0, -23)."<a href='".substr($tweet['tweet_text'], -23)."'>‌ ‌</a>" : $tweet['tweet_text'];
            $shortCodes = array('[TEXT]', '[NAME]', '[UNAME]', '[LINK]', '[CHANNEL]');
            $scValues = array($tweet['tweet_text'], $tweet['user_name'], $tweet['user_id'], 'https://twitter.com/'.$tweet['user_id'].'/status/'.$tweet['tweet_id'], substr($this->settings('targetID'), 1));
            $block = ($this->isAdmin(null, $target)>1) ? "\n\n📛 /BLK_" .$tweet['user_id'] : null;
            $caption = str_replace($shortCodes, $scValues, $this->settings('postFormat')).$block;
            if(empty($tweet['video']) && empty($tweet['photo1']))
            {
                $content = array('chat_id' => $target, 'text' => $caption, 'parse_mode'=>'html', 'disable_web_page_preview'=>$webPerview);
                $post_id = $telegram->sendMessage($content);
                $post_id = $post_id['result']['message_id'];
            }
            else if(!empty($tweet['video']))
            {
                $content = array('chat_id' => $target,'video'=> $tweet['video'], 'caption' => $caption, 'parse_mode'=>'html', 'disable_web_page_preview'=>$webPerview);
                $post_id = $telegram->sendVideo($content);
                $post_id = $post_id['result']['message_id'];
            }
            else if(empty($tweet['video']) && !empty($tweet['photo1']) && empty($tweet['photo2']))
            {
                $content = array('chat_id' => $target,'photo'=> $tweet['photo1'], 'caption' => $caption, 'parse_mode'=>'html', 'disable_web_page_preview'=>$webPerview);
                $post_id = $telegram->sendPhoto($content);
                $post_id = $post_id['result']['message_id'];
            }
            else if(empty($tweet['video']) && !empty($tweet['photo1']) && !empty($tweet['photo2']))
            {
                $photos = array_filter([$tweet['photo1'], $tweet['photo2'], $tweet['photo3'], $tweet['photo4']]);
                $i = 0;
                print_r($photos);
                foreach($photos as $photo)
                {
                    $media[$i] = array('type' => 'photo', 'media' => $photo, 'parse_mode'=>'html');
                    $media[0]['caption'] = $caption;
                    $i++;
                }
                $content = array('chat_id' => $target, 'media' => json_encode($media));
                $post_id = $telegram->sendMediaGroup($content);
                $post_id = $post_id['result'][0]['message_id'];
            }
            if($updates != null || isset($post_id))
            {
                $this->query("UPDATE tweets SET post_id=$post_id $query WHERE id=".$tweet['id']);
            }
            if($this->settings('targetID')==$target)
                $this->query("UPDATE tweets SET post_id=$post_id WHERE id=".$tweet['id']);
            if(!empty($updates))
            {
                $i=0; $query=null;
                foreach(array_keys($updates) as $key)
                    $query .= (($i++) ? "," : "" ) . "$key='". $updates[$key] ."' ";
                $this->query("UPDATE tweets SET $query WHERE id=".$tweet['id']);
            }
            return ($post_id) ? true : false;
        }
        else
            return false;
    }
    
    function close()
    {
        return $this->conn->close();
    }
}
