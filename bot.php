<?php

include 'database.php';

$db = new database();

$telegram = new Telegram($db->settings('botToken'));
$result   = $telegram->getData();

$text = $result['message']['text'];
$chatId = $result['message']['chat']['id'];
$username = $result['message']['from']['username'];
$firstName = $result['message']['from']['first_name'];
$repliedText = @$result['message']['reply_to_message']['text'];

$minPublishTime = $db->settings('minOfPublishTime');
$minRetweet     = $db->settings('minCountOfRetweet');
$minFavorite    = $db->settings('minCountOfFavorite');

$autoConfirm            = ($db->settings('autoConfirm')) ? 'بله' :  'خیر';
$activationBoolean      = $db->settings('activation');
$activation             = ($activationBoolean) ? 'فعال' : 'غیرفعال';
$numOfTweets            = $db->numRows('tweets');
$numOfConfirmedTweets   = $db->numRows('tweets', 'confirmed=1');
$numOfConfirmQueue      = $db->numRows('tweets', 'seen=0 AND confirmed=0');
$target                 = $db->settings('targetID');
$numOfPostQueue         = $db->numRows('tweets', 'seen=1 AND confirmed=1 AND is_sent=0');
$postIinterruption      = $db->settings('pauseBetweenPosts');
$numOfSimultaneousPosts = $db->settings('numOfTweetsPerPost');
$numOfCensoredWords     = $db->numRows('censored_words');
$numOfCensoredUsers     = $db->numRows('censored_users');
$postFormat             = str_replace('<', '&#60;', $db->settings('postFormat'));

$listOfAdmins = null;
foreach($db->rows('tel_admins') as $tmpUser)
    $listOfAdmins .= "\n\n🏅 آی‌دی: @".$tmpUser['username'].
        (($tmpUser['access_level']==3) ? null : "\n🗑 حذف /DA_".$tmpUser['id'].substr(md5(rand(1,1000)), 0, 10)) .
        (($tmpUser['access_level']==1) ? "\n⏫ ارتقا /PRO_".$tmpUser['id'].substr(md5(rand(1,1000)), 0, 10) : null) .
        (($tmpUser['access_level']==2) ? "\n⏬ تنزل /DEM_".$tmpUser['id'].substr(md5(rand(1,1000)), 0, 10): null)
    ;

$listOfCensoredWords = null;
foreach($db->rows('censored_words') as $tmpWord)
    $listOfCensoredWords .= "\n\nˣ <code>" . $tmpWord['word'] . "</code>\n🗑 /DW_" .
        $tmpWord['id'].substr(md5(rand(1,1000)), 0, 10)
    ;

$listOfCensoredUsers = null;
foreach($db->rows('censored_users') as $tmpUser)
    $listOfCensoredUsers .= "\n\nˣ <code>" . $tmpUser['username'] .
        "</code>\n🗑 /DU_" . $tmpUser['id'].substr(md5(rand(1,1000)), 0, 10)
    ;


define("UNK_TXT", "<b>دستور نامفهوم</b>\nدستور وارد شده مفهوم نبود، برای شروع مجدد روی /start کلیک کنید.\n‌");
define('WLC_TXT', "<b>سلام $firstName عزیز</b>\nبرای استفاده از ربات از کلیدهای زیر کمک بگیرید.\nو یا برای راهنمایی‌های بیشتر با طراح ربات در تماس باشید: \n👨‍💻 $programmer");
define("STNG_KEY", "⚙️ تنظیمات");
define("CHTW_KEY", "🔍 بررسی‌توییت");
define("OVRW_KEY", "📋 اطلاعات‌اجمالی");
define("OVRW_TXT", "❤️ حداقل فیوریت:   <b>$minFavorite</b>\n♻️ حداقل ریتوییت:  <b>$minRetweet</b>\n⏰ حداکثر قدمت:   <b>$minPublishTime</b> ساعت\n✅تایید خودکار:   <b>$autoConfirm</b>\n📑 کل توییت‌ها:   <b>$numOfTweets</b>
📁 درصف بررسی:   <b>$numOfConfirmQueue</b>\n🔖 تایید شده:   <b>$numOfConfirmedTweets</b>\n🗂 صف ارسال:   <b>$numOfPostQueue</b>\nمقصد: $target (کانال/گروه)\n⏳ وقفه بین ارسال‌ها:   <b>$postIinterruption دقیقه</b>
📤 ارسال همزمان:   <b>$numOfSimultaneousPosts توییت</b>\n🙊 کلمات سانسورشده:   <b>$numOfCensoredWords</b>\n🐒 کاربران بلاک‌شده:   <b>$numOfCensoredUsers</b>\n📊 وضعیت ربات: $activation\n‌");
define("STNG_TXT", "برای تنظیم هر مورد روی کلید مربوطه کلیک کنید. برای دیدن تنظیمات کنونی از کلید <b>اطلاعات اجمالی</b> در منوی اصلی استفاده کنید.\n‌");
define("MNMU_KEY", "🏠 منوی‌اصلی");
define("SFAV_KEY", "❤️ تنظیم فیوریت");
define("SRET_KEY", "♻️ تنظیم ریتوییت");
define("STIM_KEY", "⏰ تنظیم قدمت");
define("SPUS_KEY", "⏳ تنظیم وقفه");
define("SAUC_KEY", "✅ تایید خودکار");
define("EMPS_KEY", "🗑 تخلیه صف ارسال");
define("EMPC_KEY", "🗑 تخلیه صف تایید");
define("AUSG_KEY", "📤 تنظیم ارسال همزمان");
define("CENU_KEY", "🐒 سانسور کاربر");
define("CENW_KEY", "🙊 سانسور کلمه");
define("ADML_KEY", "📋 لیست ادمین‌ها");
define("NADM_KEY", "👤 ادمین جدید");
define("ADML_TXT", "<b>👥 لیست ادمین‌ها:</b>$listOfAdmins\n‌");
define("DELA_TXT", "✅ ادمین موردنظر با موفقیت <b>خلع</b> شد!");
define("NEWA_TXT", "آی‌دی 🆔 ادمین موردنظر را بدون @ وارد کن.\n\n⛔️ @admin\n⛔️ t.‌me/admin\n✅ admin\n‌");
define("NAAD_TXT", "ادمین جدید با موفقیت اضافه شد.\n\n⚙️ /setting\n🏠 /start\n‌");
define("NCUS_TXT", "یوزرنیم کاربر موردنظر را وارد کنید.\n\n⛔️ @google\n⛔️ twitter.‌com/google\n✅ google\n‌");
define("NUSC_TXT", "✅ کاربر موردنظر با موفقیت به لیست سانسور اضافه شد.\n\n⚙️ /setting\n🏠 /start\n‌");
define("CEUL_KEY", "👥 کاربران سانسورشده");
define("CEWL_KEY", "🗣 کلمات سانسورشده");
define("CUSL_TXT", "<b>لیست کاربران سانسورشده:</b>$listOfCensoredUsers\n\n✅ برای حذف کاربر روی کامند روبروش کلیک کنید.");
define("NCUL_TXT", "هیچ کاربری سانسور نشده.");
define("DECU_TXT", "کاربر موردنظر با موفقیت از لیست سانسورها خارج شد.");
define("CWOL_TXT", "<b>👥 لیست کلمات سانسورشده:</b>$listOfCensoredWords\n\n✅ برای حذف کلمه روی کامند روبرویش کلیک کنید.");
define("NCWL_TXT", "‼️ هیچ کلمه‌ای سانسور نشده.");
define("DELw_TXT", "✅ کلمه موردنظر با موفقیت از لیست سانسورها خارج شد.");
define("NWSW_TXT", "🗣 کلمه موردنظر برای سانسور رو وارد کنید.\n\n✅ کلمه\n✅ #هشتگ\n✅ کلمه های بلند");
define("NWCD_TXT", "✅ کلمه موردنظر با موفقیت به لیست سانسور اضافه شد.");
define("SRET_TXT", "مقدار حداقل ریتوییت موردنظر را وارد کنید:");
define("MRDO_TXT", "✅ مقدار حداقل ریتوییت با موفقیت بروزرسانی شد.\n\n⚙️ /setting\n🏠 /start\n‌");
define("SEFA_TXT", "مقدار حداقل فیوریت موردنظر را وارد کنید:");
define("MSDO_TXT", "✅ مقدار حداقل فیوریت  با موفقیت بروزرسانی شد.\n\n⚙️ /setting\n🏠 /start\n‌");
define("SETI_TXT", "مقدار حداکثر قدمت موردنظر (ساعت) را وارد کنید:");
define("MADO_TXT", "✅ مقدار حداکثر قدمت با موفقیت بروزرسانی شد.\n\n⚙️ /setting\n🏠 /start\n‌");
define("SEPA_TXT", "مقدار وقفه (دقیقه) بین ارسال‌های متوالی به کانال/گروه را وارد کنید:");
define("PADO_TXT", "✅ مقدار وقفه بین ارسال‌ها با موفقیت بروزرسانی شد.\n\n⚙️ /setting\n🏠 /start\n‌");
define("SSGN_TXT", "تعداد توییت‌هایی که در هربار ارسال بصورت همزمان ارسال میشوند را وارد کنید:\n(اکیدا پیشنهاد می‌شود مقداری بیش از ۱۰ را وارد نکنید)");
define("SSGD_TXT", "✅ مقدار تعداد ارسال همزمان با موفقیت بروزرسانی شد.\n\n⚙️ /setting\n🏠 /start\n‌");
define("DIAC_TXT", "⛔️ /AC_OFF - غیرفعالسازی تایید خودکار\n\n🔰 درصورت فعال بودن، توییت‌ها بدون نیاز به تایید دستی وارد صف ارسال به کانال/گروه میشن.");
define("ENAC_TXT", "✅ /AC_ON  - فعالسازی تایید خودکار\n\n🔰 درصورت فعال بودن، توییت‌ها بدون نیاز به تایید دستی وارد صف ارسال به کانال/گروه میشن.");
define("SECH_KEY", "🆔 تنظیم کانال/گروه مقصد");
define("SECH_TXT", "🆔 مقصد کنونی: $target\n\n1️⃣ درصورتی که مقصد موردنظر یک کانال است، آی‌دی کانال را با علامت @ وارد کنید.\n\n2️⃣ اگر مقصد مورد نظر یک گروه است، آی‌دی عددی گروه را با علامت - وارد کنید.");
define("TADO_TXT", "✅ کانال/گروه مقصد با موفقیت بروزرسانی شد.\n\n⚙️ /setting\n🏠 /start\n‌");
define("EMCD_CMD", "/EMPSND");
define("EMSQ_TXT", "برای تخلیه صف ارسالی به کانال/گروه مقصد، روی کامند /EMPSND کلید کنید.");
define("ESQD_TXT", "✅ صف ارسال با موفقیت تخلیه شد.");
define("EMCO_CMD", "/EMPCNF");
define("EMQI_TXT", "برای تخلیه صف تایید روی کامند /EMPCNF کلیک کنید.");
define("EMCD_TXT", "✅ صف تایید با موفقیت تخلیه شد.");
define("SEFO_KEY", "📃 فرمت پست‌ها");
define("TFHE_TXT", "<b>📃 راهنمای فرمت پست‌ها:</b>\n\n1️⃣<b> بولد کردن متن:</b><code>\n&#60;b>Text&#60;/b></code>\n2️⃣<b> ساخت لینک:</b>\n<code>&#60;a href='LINK'>TEXT&#60;/a></code>
3️⃣<b> شورت‌کدها:</b>
متن توییت : <code>[TEXT]</code>
نام کاربر : <code>[NAME]</code>
یوزرنیم کاربر : <code>[UNAME]</code>
لینک توییت : <code>[LINK]</code>
آی‌دی کانال (بدون @) : <code>[CHANNEL]</code>\n
فرمت کنونی: \n----------\n<code>$postFormat</code>\n----------");
define("PFUP_TXT", "✅ فرمت پست‌ها با موفقیت آپدیت شد.");
define("SETF_TXT", "برای تنظیم فرمت، فرمت موردنظر را ارسال کنید:");
define("COKY_KEY", "✅ تایید توییت");
define("QUIN_KEY", "⬅️ ارسال فوری");
define("RECE_TXT", "برای تایید، ارسال فوری و یا نادیده گرفتن هر توییت، از کلید های زیر استفاده کنید.\nاولین توییت تا ۵ ثانیه آینده ارسال خواهد شد.");
define("NETW_KEY", "توییت بعدی");
define("TWCO_TXT", "✅ توییت موردنظر تائید شد.");
define("TWST_TXT", "✅ توییت با موفقیت به کانال ارسال شد.");
define("DISB_KEY", "🔒 غیرفعالسازی ربات");
define("ENAB_KEY", "🔓 فعال‌سازی ربات");
define("RESB_KEY", "🔂 ریست ربات");
define("RESB_TXT", "با ریست ربات، همه توییت ها اعم از توییت های در صف بررسی و توییت های درصف ارسال، حذف خواهند شد.\nتنظیمات تغییر نخواهند کرد. برای ریست روی /reset کلیک کنید.");
define("BRES_TXT", "✅ ربات با موفقیت ریست شد.");
define("NOQU_TXT", "‼️ درحال حاضر هیچ توییتی در صف بررسی وجود ندارد.");
define("DIRO_TXT", "✅ ربات درحال حاضر فعال است.\nبرای غیرفعال سازی ربات، روی کامند /StopRobot کلیک کنید.\n‌");
define("ACRO_TXT", "📛 ربات درحال حاضر غیر فعال است.\nبرای فعالسازی ربات، روی کامند /StartRobot کلیک کنید.\n‌");
define("RACD_TXT", "🔓 ربات با موفقیت فعال شد.");
define("RODS_TXT", "🔒 ربات با موفقیت غیرفعال شد.");
define("LICE_KEY", "🥇 اطلاعات‌لایسنس");
define("AACT_KEY", "🗂 عملکرد ادمین‌ها");
define("NOPE_TXT", "هیج توییتی انتخاب نشده است! لطفا صبر کنید تا اولین توییت ارسال شود.");
define("CAOW_TXT", "📛 شما نمی‌توانید مالک ربات را خلع کنید!");
define("ACEN_TXT", "✅ تایید خودکار فعال شد.");
define("ACDS_TXT", "✅ تایید خودکار غیرفعال شد.");
define("NOST_TXT", "📛 هیچ آماری وجود ندارد!");
define('ALST_TXT', "✅  ادمین موردنظر اخطار دریافت کرد.");
define('RECV_TXT', "⛔️ هشدار!\nشما بابت تایید توییت بالا یک اخظار دریافت کرده اید. لطفا توییت‌های مشابه را تایید نکنید.");
define('DEMO_TXT', "✅  ادمین موردنظر با موفقیت تنزل مقام یافت.");
define('PROM_TXT', "✅  ادمین موردنظر با موفقیت ارتقا مقام یافت.");

if($db->isAdmin($username, $chatId))
{
    $db->updateTable('tel_admins', 'user_id', $chatId, 'username', strtolower($username));
    $db->updateTable('tel_admins', 'username', strtolower($username), 'user_id', $chatId);
    
    if($text == '/StopRobot' || $text == '/StartRobot') $activationBoolean = 1 - $activationBoolean;
    $activationKey = ($activationBoolean) ? DISB_KEY : ENAB_KEY;
    
    $keys = [
                [$telegram->buildKeyboardButton(CHTW_KEY)], 
                [$telegram->buildKeyboardButton(OVRW_KEY),
                $telegram->buildKeyboardButton(AACT_KEY)],
            ]
    ;
    
    if($db->isAdmin($username, $chatId)>1)
        $keys = [
                    [$telegram->buildKeyboardButton(STNG_KEY), $telegram->buildKeyboardButton(CHTW_KEY)], 
                    [$telegram->buildKeyboardButton(OVRW_KEY), $telegram->buildKeyboardButton(AACT_KEY)],
                    [$telegram->buildKeyboardButton($activationKey), $telegram->buildKeyboardButton(RESB_KEY)],
                    [$telegram->buildKeyboardButton(LICE_KEY)]
                ]
        ;
        
    $mainMenuKeys = $telegram->buildKeyBoard($keys, $onetime=false);
    
    switch($text)
    {
        case '/start':
        case '/Start':
        case MNMU_KEY:
            $db->updateTable('tel_admins', 'confirm_pending_id', '', 'username', strtolower($username));
            $msg = WLC_TXT;
            $content = array('chat_id' => $chatId, 'reply_markup' => $mainMenuKeys, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;
        
        case OVRW_KEY:
            $msg = OVRW_TXT;
            $content = array('chat_id' => $chatId, 'reply_markup' => $mainMenuKeys, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case STNG_KEY:
        case '/setting':
            $msg = STNG_TXT;
            $keys = [
                        [$telegram->buildKeyboardButton(SECH_KEY), $telegram->buildKeyboardButton(SEFO_KEY)],
                        [$telegram->buildKeyboardButton(SFAV_KEY), $telegram->buildKeyboardButton(SRET_KEY)], 
                        [$telegram->buildKeyboardButton(STIM_KEY), $telegram->buildKeyboardButton(SPUS_KEY)], 
                        [$telegram->buildKeyboardButton(SAUC_KEY), $telegram->buildKeyboardButton(AUSG_KEY)], 
                        [$telegram->buildKeyboardButton(EMPC_KEY), $telegram->buildKeyboardButton(EMPS_KEY)], 
                        [$telegram->buildKeyboardButton(CENU_KEY), $telegram->buildKeyboardButton(CENW_KEY)], 
                        [$telegram->buildKeyboardButton(CEUL_KEY), $telegram->buildKeyboardButton(CEWL_KEY)], 
                        [$telegram->buildKeyboardButton(NADM_KEY), $telegram->buildKeyboardButton(ADML_KEY)], 
                        [$telegram->buildKeyboardButton(MNMU_KEY)]
                    ]
            ;
            $keyboard = $telegram->buildKeyBoard($keys, $onetime=false);
            $content = array('chat_id' => $chatId,'reply_markup' => $keyboard, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;
        
        case AACT_KEY:
            $admins = $db->adminStat();
            if(!$admins)
                $msg = NOST_TXT;
            else
            {
                $i = 1;
                $msg = null;
                foreach($admins as $admin)
                {
                    $msg .= "\n\n$i. یوزرنیم ادمین: @".$admin['username']."\nتعداد مشاهده: ".$admin['seen']."\nتعداد تایید: ".$admin['confirmed']."\nدرصد تایید: ".$admin['confirm_percentage'];
                    $i++;
                }
            }
            $content = array('chat_id' => $chatId, 'reply_markup' => $mainMenuKeys, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;
        
        case ADML_KEY:
            $msg = ADML_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case NADM_KEY:
            $msg = NEWA_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html', 'reply_markup'=>json_encode(array('force_reply'=>true)));
            $telegram->sendMessage($content);
        break;
        
        case CEUL_KEY:
            $msg = ($numOfCensoredUsers!=0) ? CUSL_TXT : NCUL_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case CENU_KEY:
            $msg = NCUS_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html', 'reply_markup'=>json_encode(array('force_reply'=>true)));
            $telegram->sendMessage($content);
        break;

        case CEWL_KEY:
            $msg = ($numOfCensoredWords!=0) ? CWOL_TXT : NCWL_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case CENW_KEY:
            $msg = NWSW_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html', 'reply_markup'=>json_encode(array('force_reply'=>true)));
            $telegram->sendMessage($content);
        break;
        
        case SRET_KEY:
            $msg = SRET_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html', 'reply_markup'=>json_encode(array('force_reply'=>true)));
            $telegram->sendMessage($content);
        break;

        case SFAV_KEY:
            $msg = SEFA_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html', 'reply_markup'=>json_encode(array('force_reply'=>true)));
            $telegram->sendMessage($content);
        break;

        case STIM_KEY:
            $msg = SETI_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html', 'reply_markup'=>json_encode(array('force_reply'=>true)));
            $telegram->sendMessage($content);
        break;

        case SPUS_KEY:
            $msg = SEPA_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html', 'reply_markup'=>json_encode(array('force_reply'=>true)));
            $telegram->sendMessage($content);
        break;

        case AUSG_KEY:
            $msg = SSGN_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html', 'reply_markup'=>json_encode(array('force_reply'=>true)));
            $telegram->sendMessage($content);
        break;

        case SAUC_KEY:
            $msg = ($db->settings('autoConfirm')) ? DIAC_TXT : ENAC_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case SECH_KEY:
            $msg = SECH_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html', 'reply_markup'=>json_encode(array('force_reply'=>true)));
            $telegram->sendMessage($content);
        break;

        case EMPS_KEY:
            $msg = EMSQ_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case EMCD_CMD:
            $db->updateTable('tweets', 'is_sent', 1, 'confirmed', 1);
            $msg = ESQD_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case EMPC_KEY:
            $msg = EMQI_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case EMCO_CMD:
            $db->updateTable('tweets', 'seen', 1, false, false);
            $msg = EMCD_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case SEFO_KEY:
            $msg = TFHE_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
            $msg = SETF_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html', 'reply_markup'=>json_encode(array('force_reply'=>true)));
            $telegram->sendMessage($content);
        break;
            
        case CHTW_KEY:
            if($numOfConfirmQueue == 0)
            {
                $msg = NOQU_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
                break;
            }
            $keys = [
                        [$telegram->buildKeyboardButton(QUIN_KEY), $telegram->buildKeyboardButton(COKY_KEY)], 
                        [$telegram->buildKeyboardButton(NETW_KEY)],
                        [$telegram->buildKeyboardButton(MNMU_KEY)]
                    ]
            ;
            $keyboard = $telegram->buildKeyBoard($keys, $onetime=false);
            $msg = RECE_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html', 'reply_markup' => $keyboard);
            $telegram->sendMessage($content);
            sleep(4);
        case NETW_KEY:
            $tweet = $db->getTweet('bot');
            if($numOfConfirmQueue == 0)
            {
                $msg = NOQU_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
                break;
            }
            $db->updateTable('tel_admins', 'confirm_pending_id', $tweet['id'], 'username', strtolower($username));
            $db->sendToTelegram($chatId, 'bot', ['seen'=>1, 'admin'=>$chatId]);
        break;

        case COKY_KEY:
            $confirmPendingID = $db->getConfirmPendingID($username);
            if(empty($confirmPendingID))
            {
                $msg = NOPE_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
                break;
            }
            $db->updateTable('tweets', 'confirmed', '1', 'id', $confirmPendingID);
            $msg = TWCO_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case QUIN_KEY:
            $num = $db->getConfirmPendingID($username);
            if(empty($num))
            {
                $msg = NOPE_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
                break;
            }
            $db->sendToTelegram($target, $num, ['is_sent'=>1, 'confirmed'=>1]);
            $msg = TWST_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case RESB_KEY:
            $msg = RESB_TXT;
            $content = array('chat_id' => $chatId, 'reply_markup' => $mainMenuKeys, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case '/reset':
            $db->truncateTable('tweets');
            foreach($db->rows('tel_admins', 'id') as $id)
                $db->updateTable('tel_admins', 'confirm_pending_id', NULL, 'id', $id);
            $msg = BRES_TXT;
            $content = array('chat_id' => $chatId, 'reply_markup' => $mainMenuKeys, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case DISB_KEY:
        case ENAB_KEY:
            $msg = ($activationBoolean==0) ? ACRO_TXT: DIRO_TXT;
            $content = array('chat_id' => $chatId, 'reply_markup' => $mainMenuKeys, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;  

        case '/StopRobot':
            $db->settings('activation', '0');
            $msg = RODS_TXT;
            $content = array('chat_id' => $chatId, 'reply_markup' => $mainMenuKeys, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;
        
        case '/StartRobot':
            $db->settings('activation', '1');
            $msg = RACD_TXT;
            $content = array('chat_id' => $chatId, 'reply_markup' => $mainMenuKeys, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

        case LICE_KEY:
            $permium = json_decode(file_get_contents("$apiURL?planInfo"), true);
            if($permium)
                $msg = ($permium['error']) ? $permium['error_message'] : $permium['fa']['msg'];
            else
                $msg = "Connection Error, Url:\n $apiURL?planInfo\n\nPlease try later.";
            $content = array('chat_id' => $chatId, 'reply_markup' => $mainMenuKeys, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        break;

    default:
        if($db->isAdmin($username, $chatId)>1)
        {
            if(substr($text, 0, 4) == '/DA_')
            {
                $adminID = substr($text, 4, -10);
                if($db->isAdmin(null, null, $adminID)==3)
                {
                    $msg = CAOW_TXT;
                }
                else
                {
                    $db->deleteRow('tel_admins', 'id', $adminID);
                    $msg = DELA_TXT;
                }
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if(substr($text, 0, 4) == '/DU_')
            {
                $id = substr($text, 4, -10);
                $db->deleteRow('censored_users', 'id', $id);
                $msg = DECU_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if(substr($text, 0, 4) == '/DW_')
            {
                $id = substr($text, 4, -10);
                $db->deleteRow('censored_words', 'id', $id);
                $msg = DELw_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if(substr($text, 0, 4) == '/AC_')
            {
                $autoConfirm = substr($text, 4);
                ($autoConfirm == 'ON') ? $db->settings('autoConfirm', '1') : $db->settings('autoConfirm', '0');
                $msg = ($autoConfirm == 'ON') ? ACEN_TXT : ACDS_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if(substr($text, 0, 5) == '/BLK_')
            {
                $db->addRecord('censored_users', array('username'=>strtolower(substr($text, 5))));
                $msg = NUSC_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if(substr($text, 0, 7) == '/ALERT_')
            {
                $IDs = explode('_', substr($text, 7));
                $alertTarget = $IDs[1];
                $tweetID = $IDs[0];
                $db->sendToTelegram($alertTarget, $tweetID);
                $msg = RECV_TXT;
                $content = array('chat_id' => $alertTarget, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
                $msg = ALST_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if(substr($text, 0, 5)=='/DEM_')
            {
                $id = substr($text, 5, -10);
                $db->updateTable('tel_admins', 'access_level', 1, 'id', $id);
                $msg = DEMO_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if(substr($text, 0, 5)=='/PRO_')
            {
                $id = substr($text, 5, -10);
                $db->updateTable('tel_admins', 'access_level', 2, 'id', $id);
                $msg = PROM_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if($repliedText == NEWA_TXT)
            {
                $db->addRecord('tel_admins', array('username'=>strtolower(str_replace('@','',$text))));
                $msg = NAAD_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if($repliedText == NCUS_TXT)
            {
                $db->addRecord('censored_users', array('username'=>strtolower($text)));
                $msg = NUSC_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if($repliedText == NWSW_TXT)
            {
                $db->addRecord('censored_words', array('word'=>$text));
                $msg = NWCD_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if($repliedText == SRET_TXT)
            {
                $db->settings('minCountOfRetweet', $text);
                $msg = MRDO_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if($repliedText == SEFA_TXT)
            {
                $db->settings('minCountOfFavorite', $text);
                $msg = MSDO_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if($repliedText == SETI_TXT)
            {
                $db->settings('minOfPublishTime', $text);
                $msg = MADO_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if($repliedText == SEPA_TXT)
            {
                $db->settings('pauseBetweenPosts', $text);
                $msg = PADO_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if($repliedText == SSGN_TXT)
            {
                $db->settings('numOfTweetsPerPost', $text);
                $msg = SSGD_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if($repliedText == FOLLOW_NEW_USER_TEXT)
            {
                $db->addRecord('following', array('username'=>$text));
                $msg = NEW_USER_HAS_BEEN_FOLLOWED_TEXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if($repliedText == SECH_TXT)
            {
                $db->settings('targetID', $text);
                $msg = TADO_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if($repliedText == SETF_TXT)
            {
                $db->settings('postFormat', str_replace("'", "\'", $text));
                $msg = PFUP_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
            else if('@'.$result['message']['forward_from_chat']['username']==$target )
            {
                $msg = "🔗 لینک پست:\nhttps://t.me/".$result['message']['forward_from_chat']['username']."/".$result['message']['forward_from_message_id'];
                $tweet = $db->getTweet(null, ['post_id'=>$result['message']['forward_from_message_id']]);
                $msg .= ($tweet['admin']!=null) ?
                    "\n\n👨‍🏫 ادمین تاییدکننده:\n<a href='tg://user?id=".$tweet['admin']. "'>-> @".$db->aboutAdmin(null, $tweet['admin'])['username']."</a>".
                    "\n\n 📣 اخطار به ادمین:\n/ALERT_".$tweet['id']."_".$tweet['admin']
                    :
                    "\n\n🤖 این توییت بصورت خودکار توسط ربات تایید شده‌است.";
                $msg .= "\n\n📛 بلاک کردن کاربر:\n/BLK_".$tweet['user_id'];
                    $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html', 'disable_web_page_preview'=>true);
                $telegram->sendMessage($content);
            }
            else
            {
                $msg = UNK_TXT;
                $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
                $telegram->sendMessage($content);
            }
        }
        else
        {
            $msg = UNK_TXT;
            $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
            $telegram->sendMessage($content);
        }
    }
}
else
{
    $msg = "<b>403 - Access is Denied!</b>\nmore information: $programmer\n‌";
    $content = array('chat_id' => $chatId, 'text' => $msg, 'parse_mode'=>'html');
    $telegram->sendMessage($content);
}

$db->close();
