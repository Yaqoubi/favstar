<?php
include 'database.php';
$db = new database();

if($db->settings('activation'))
{
    $tweets = json_decode(file_get_contents($apiURL), true);
    if(empty($tweets['error']) && !empty($tweets[0]))
    {
        foreach($tweets as $tweet)
        {
            $name        = $tweet['user']['name'];
            $idStr      = $tweet['id'];
            $variants    = @$tweet['extended_entities']['media'][0]['video_info']['variants'];
            $verified    = ($tweet['user']['verified']) ? 1 : 0;
            $fullText   = $tweet['full_text'];
            $createdAt  = $tweet['created_at'];
            $screenName = $tweet['user']['screen_name'];
            $retweetCount   = $tweet['retweet_count'];
            $favoriteCount  = $tweet['favorite_count'];
            $isQuoteStatus  = ($tweet['is_quote_status']) ? 1 : 0;
            $photo1 = @$tweet['extended_entities']['media'][0]['media_url'];
            $photo2 = @$tweet['extended_entities']['media'][1]['media_url'];
            $photo3 = @$tweet['extended_entities']['media'][2]['media_url'];
            $photo4 = @$tweet['extended_entities']['media'][3]['media_url'];
            $urls   = $tweet['entities']['urls'];
            if(count($urls) > 0 && !$isQuoteStatus)
                foreach($urls as $url)
                    $fullText = str_replace($url['url'], $url['expanded_url'], $fullText);
            $video  = null;
            if(!empty($variants))
                for($i=0, $bitrate=0 ; $i < count($variants) ; $i++)
                    if(@$variants[$i]['bitrate'] > $bitrate && @$variants[$i]['content_type'] == 'video/mp4')
                    {
                        $video   = $variants[$i]['url'];
                        $bitrate = $variants[$i]['bitrate'];
                    }
            $autoConfirm = ($db->settings('autoConfirm')) ? 1 : 0;
            if(
                $db->dateCmp($createdAt) &&
                (
                    $favoriteCount >= $db->settings('minCountOfFavorite') || 
                    $retweetCount >= $db->settings('minCountOfRetweet')
                ) && 
                $db->isFreshTweet($idStr) &&
                $db->censorship($screenName, $fullText)
            )
            {
                $db->addRecord('tweets', 
                    [
                        'tweet_id'=>$idStr,
                        'tweet_text'=>str_replace("'", '"', $fullText),
                        'user_name'=>str_replace("'", '"', $name),
                        'user_id'=>$screenName,
                        'is_quote'=>$isQuoteStatus,
                        'photo1'=>$photo1,
                        'photo2'=>$photo2,
                        'photo3'=>$photo3,
                        'photo4'=>$photo4,
                        'video'=>$video,
                        'verified'=>$verified,
                        'admin'=> null,
                        'confirmed'=>$autoConfirm,
                        'seen'=>$autoConfirm,
                        'is_sent'=> 0
                    ]
                );
            }
        }
    }
}
else
    echo "BOT IS DIACTIVATED.";
$db->close();
?>